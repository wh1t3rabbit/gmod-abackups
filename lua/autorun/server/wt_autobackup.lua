
--
local get_path = string.GetPathFromFilename
local file_exists = file.Exists
local file_read = file.Read
local file_write = file.Write
local isdir = file.IsDir
local mkdir = file.CreateDir

--Tag for unqiue hook names
local TAG = "autobackup:"
local DATA = "DATA"

--Config
local BACKUP_DIR = "_abackups" --Where to store our backups
local filesToBackup = {
	--File paths relative to the data directory
	--eg. to backup garrysmod/data/ulib/groups.txt the entry would just be ulib/groups.txt
	["ulib/groups.txt"] = true,
	["ulib/users.txt"] = true,
}

--Helper function to recursively create folders until the full path exists
local function UTIL_EnsureFolderExists(path)
	--print(TAG,"UTIL_EnsureFolderExists",path)
	local parts = string.Explode("/",path)
	local tempPath = ""
	while #parts>0 do
		local piece = table.remove(parts,1)
		tempPath = tempPath..piece
		--print(TAG,"UTIL_EnsureFolderExists","Check if folder exists",tempPath)
		if not isdir(tempPath,DATA) then
			--print(TAG,"UTIL_EnsureFolderExists","Creating folder",tempPath)
			mkdir(tempPath)
		end
		tempPath = tempPath.."/"
	end
end

--The function that does stuff
local function BackupTheFiles()
	print(TAG,"Backup started")
	--Create a folder for this backup
	local timestamp = util.DateStamp()
	local BaseFolder = BACKUP_DIR.."/"..timestamp
	print(TAG,"Store this backup in",BaseFolder)
	UTIL_EnsureFolderExists(BaseFolder)
	BaseFolder = BaseFolder.."/"
	--Loop the entries in the config
	for filepath,dobackup in pairs(filesToBackup) do
		--Skip the file if we've been told not to back it up
		if not dobackup then
			print(TAG,"Skipping file",filepath)
			continue
		end
		--Make sure the file exists
		if not file_exists(filepath,DATA) then
			print(TAG,"File doesn't exist",filepath)
			continue
		end
		--Create the output folder for it if needed
		local outputPath = BaseFolder..filepath
		local outputFolder = get_path(outputPath)
		print(TAG,"Backup",filepath,"to",outputPath)
		UTIL_EnsureFolderExists(outputFolder)
		--Now read filepath and write it to outputPath
		file_write( outputPath, file_read(filepath, DATA) )
	end
	print(TAG,"Backup finished")
end

--Backup on shutdown
hook.Add("ShutDown", TAG.."Shutdown", function()
	print(TAG,"Shutting down, backup files")
	BackupTheFiles()
end)

--Startup
UTIL_EnsureFolderExists(BACKUP_DIR)
timer.Simple(0, function()
	print(TAG,"Starting up, backup files")
	BackupTheFiles()
end)

print(TAG,"Loaded")